const yaml = require("js-yaml");

module.exports = function(eleventyConfig) {
    // Output directory: _site
    eleventyConfig.setTemplateFormats("html,njk");
    eleventyConfig.addDataExtension("yaml", contents => yaml.load(contents));

};