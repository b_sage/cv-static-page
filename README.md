<img src="res/screenshot.png" alt="" >

<h1 style="text-align: center"> Resume Template</h1>

After having trouble tracking the changes I made to my resume. I decide to use git to track the changes, as any sensible person would do. My first choice was to use markdown plus a static site generator with a theme. I choose markdown because it's easy to use, easy to track with git and I can use a static site generator to style and convert it to HTML. The problem was I couldn't find a good looking template. So I decided to make my own...and that's how the story begins!

### Features
- Exportable as PDF file 
- Responsive site
- Made with :heart: and frustration

Made with Eleventy, Tailwind CSS and hosted on Gitlab pages. 